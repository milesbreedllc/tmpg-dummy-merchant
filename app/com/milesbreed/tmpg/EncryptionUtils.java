/*
 * The MIT License
 *
 * Copyright 2017 Milesbreed LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software,  and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.milesbreed.tmpg;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by User on 31.01.2017.
 * Contains common encryption methods.
 */
public class EncryptionUtils {

    public static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * 3DES keys shall contain 3 8-byte DES keys and be 24 bytes long.
     * However variants with 16 byte key is possible (in this case first key part is used twice)
     * This method checks and epands short des key to full 24 bytes
     * @param keyBytes 3DES Key, either 24 or 16 bytes long
     * @return 3DES key 24 bytes long
     */
    public static byte[] expandDESKey(byte[] keyBytes) {
        if(keyBytes.length == 24) {
            return keyBytes;
        } else {
            byte[] newKey;
            if(keyBytes.length == 16) {
                newKey = new byte[24];
                System.arraycopy(keyBytes, 0, newKey, 0, 16);
                System.arraycopy(keyBytes, 0, newKey, 16, 8);
                return newKey;
            } else if(keyBytes.length == 8) {
                newKey = new byte[24];
                System.arraycopy(keyBytes, 0, newKey, 0, 8);
                System.arraycopy(keyBytes, 0, newKey, 8, 8);
                System.arraycopy(keyBytes, 0, newKey, 16, 8);
                return newKey;
            } else {
                throw new IllegalArgumentException("Key length should be 24 or 16");
            }
        }
    }

    /**
     * Creates a 3DES cipther with correct settings
     */
    public static Cipher getCipher(byte[] keyBytes, int cipherMode, boolean padding) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
        SecretKeySpec key = new SecretKeySpec(keyBytes, "DESede");
        Cipher decipher = Cipher.getInstance("DESede/ECB/" + (padding?"PKCS5Padding":"NoPadding"));
        decipher.init(cipherMode, key);
        return decipher;
    }

    public static byte[] encode3DES(byte[] keyBytes, String hexString, boolean padding) throws Exception {
        byte[] bytes = hexStringToByteArray(hexString);
        return encode3DES(keyBytes, bytes, padding);
    }

    public static byte[] encode3DES(byte[] keyBytes, byte[] bytes, boolean padding) throws Exception {
        Cipher decipher = getCipher(keyBytes, 1, padding);
        return decipher.doFinal(bytes);
    }

    public static byte[] hexStringToByteArray(String s) {
        if(s != null && !s.isEmpty()) {
            int len = s.length();
            byte[] data = new byte[len / 2];

            for(int i = 0; i < len; i += 2) {
                data[i / 2] = (byte)((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
            }

            return data;
        } else {
            return null;
        }
    }

    public static String bytesToHex(byte[] bytes) {
        if(bytes != null && bytes.length != 0) {
            char[] hexChars = new char[bytes.length * 2];

            for(int j = 0; j < bytes.length; ++j) {
                int v = bytes[j] & 255;
                hexChars[j * 2] = hexArray[v >>> 4];
                hexChars[j * 2 + 1] = hexArray[v & 15];
            }

            return new String(hexChars);
        } else {
            return "";
        }
    }
}
