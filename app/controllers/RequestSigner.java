/*
 * The MIT License
 *
 * Copyright 2017 Milesbreed LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software,  and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * Created with IntelliJ IDEA.
 * User: Egor.Smirnov
 * Date: 17.07.14
 * Time: 16:28
 */
package controllers;

import org.apache.commons.codec.digest.DigestUtils;
import play.Logger;

import java.util.Map;
import java.util.SortedMap;

/**
 * Signs request parameters.
 * Parameters are sorted according to key names in ascending order, are written down in form key=value; with ';' delimiter
 * Then a secret is appended to this, and MD5 is taken from the whole string
 */
public class RequestSigner
{
    public static String sign(SortedMap<String, String> parameters, String secret)
    {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> s : parameters.entrySet()) {
            sb.append(s.getKey()).append("=").append(s.getValue()).append(";");
        }
        sb.append(secret);
        Logger.info("Signing string");
        Logger.info(sb.toString());
        return DigestUtils.md5Hex(sb.toString());
    }

    public static boolean validateSignature(SortedMap<String, String> parameters, String secret, String signature)
    {
        final String sign = sign(parameters, secret);
        return sign.equals(signature);
    }
}
