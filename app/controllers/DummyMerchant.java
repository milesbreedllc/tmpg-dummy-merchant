/*
 * The MIT License
 *
 * Copyright 2017 Milesbreed LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software,  and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package controllers;

import com.google.gson.Gson;
import com.milesbreed.tmpg.EncryptionUtils;
import models.RequestData;
import models.RequestType;
import play.Logger;
import play.data.validation.Required;
import play.mvc.Controller;
import play.mvc.Router;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: User
 * Date: 16.07.14
 */
public class DummyMerchant extends Controller
{

    private static Random random = new Random();

    public static void failed()
    {
        render();
    }

    public static void success()
    {
        render();
    }

    public static void index()
    {
        String paymentURL = "http://172.16.0.16:8081/TMPG/PaymentProcessor";
        String merchantID = "1029299339";
        String terminalID = "199872";
        String merchantSecret = "1736f161a53472b3bbcf0bf06e84de7e";
        render(paymentURL, merchantID, terminalID, merchantSecret);
    }

    public static void pay(
            @Required String paymentURL
            , @Required String merchantId
            , @Required String secret
            , @Required String terminalId
            , @Required String memo
            , double amount
            , @Required RequestType requestType
    ) throws Exception {

        StringBuilder merchantReference = new StringBuilder(random.nextInt(9) + 1);
        for (int i = 1; i < 15; ++i) {
            merchantReference.append(random.nextInt(10));
        }

        if (requestType == RequestType.SIGNATURE) {
            SortedMap<String, String> requestParameterMap = new TreeMap<String, String>();
            requestParameterMap.put("merchantId", merchantId);
            requestParameterMap.put("terminalId", terminalId);
            requestParameterMap.put("memo", memo);
            requestParameterMap.put("totalPrice", NumberFormat.getNumberInstance(Locale.ENGLISH).format(amount));
            requestParameterMap.put("merchantReference", merchantReference.toString());
            requestParameterMap.put("notificationURL", Router.getFullUrl("DummyMerchant.notify"));
            requestParameterMap.put("successURL", Router.getFullUrl("DummyMerchant.success"));
            requestParameterMap.put("failURL", Router.getFullUrl("DummyMerchant.failed"));


            String signature = RequestSigner.sign(requestParameterMap, secret);
            String request = String.format(paymentURL + "/doPayment?terminalId=%s&merchantId=%s&memo=%s&totalPrice=%s&merchantReference=%s&notificationURL=%s&successURL=%s&failURL=%s&signature=%s"
                    , terminalId
                    , merchantId
                    , requestParameterMap.get("memo")
                    , String.valueOf(amount)
                    , merchantReference
                    , requestParameterMap.get("notificationURL")
                    , requestParameterMap.get("successURL")
                    , requestParameterMap.get("failURL")
                    , signature
            );
            redirect(request);
        } else {
            // use a secureDoPayment endpoint that needs a single 3DES encrypted string with all params

            // first fill DTO
            RequestData requestData = new RequestData();
            requestData.merchantId = merchantId;
            requestData.terminalId = terminalId;
            requestData.memo = memo;
            requestData.totalPrice = amount;
            requestData.merchantReference = merchantReference.toString();
            requestData.notificationURL = Router.getFullUrl("DummyMerchant.notify");
            requestData.successURL = Router.getFullUrl("DummyMerchant.success");
            requestData.failURL = Router.getFullUrl("DummyMerchant.failed");

            // serialize it to json string
            final String requestString = new Gson().toJson(requestData);

            // encrypt with DESede/ECB/PKCS5Padding
            final byte[] keyBytes = EncryptionUtils.expandDESKey(EncryptionUtils.hexStringToByteArray(secret));
            final String encryptedRequest = EncryptionUtils.bytesToHex(EncryptionUtils.encode3DES(keyBytes, requestString.getBytes("UTF-8"), true));

            // redirect client to TMPG url
            final String request = String.format(paymentURL + "/secureDoPayment?merchantId=%s&encryptedRequest=%s", merchantId, encryptedRequest);
            redirect(request);
        }
    }

    public static void notify(String message, String returnCode, String merchantReference, String signature)
    {
        Logger.info("Received message=%s, returnCode=%s, merchantReference=%s, signature=%s", message, returnCode, merchantReference, signature);
        ok();
    }
}
