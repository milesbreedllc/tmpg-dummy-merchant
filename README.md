# TMPG Dummy Merchant#

This repository contains a sample web application that shows how to integrate with Teasy Mobile Payment Gateway

## Prerequisites ##

Dummy Merchant app is created with Java and Play! Framework 1.4. Grab it at [https://downloads.typesafe.com/play/1.4.3/play-1.4.3.zip](https://downloads.typesafe.com/play/1.4.3/play-1.4.3.zip)

## Building and Running ##

After cloning the repository run following commands:

* play dependencies - will download and install any necessary 3rd party dependencies for the app
* play idealize / play eclipsify - will generate project files for Intellij Idea or Eclipse if you need them
* play run - will start the app

After app is started it can be accessed at http://localhost:9000

App can be also built into a .war file and deployed to any servlet container like Tomcat. Please check [Play! deployment manual](https://www.playframework.com/documentation/1.4.x/deployment) to view all possible deployment options.
You can download a prebuilt .war file from downloads section of this repository.

## Using ##

On Dummy Merchant page you will see a web form which contains necessary payment parameters:

* Payment gateway URL:	This is the URL for a TMPG doPayment method. It will be used to *redirect client browser*, so it shall be a publicly available IP address. Most likely it shall be something like http://<address>/TMPG/PaymentProcessor/doPayment
* Merchant id:	This is your store id assigned by Teasy
* Terminal id:	This is id of a terminal assigned by Teasy
* Merchant secret: A secret key provided by Teasy that is used to sign requests
* Memo: Optional string comment, client will see it on the payment page to know what he is paying for.
* Amount: Transaction amount as double
* Endpoint: TMPG endpoint. There are 2 request methods supported, one (doPayment) uses MD5 request signature while another (secureDoPayment) encrypts all request parameters using 3DES, so that client will not be able to see them in his browser address bar.

After you press the "Buy" button the Dummy Merchant app will form and sign the request and redirect you to the TMPG payment page. After payment is successful, you will be redirected back

## Integration Code ##

You can see all the code needed in DummyMerchant.java file
3DES related code can be found in EncryptionUtils class. This code is MIT licensed so feel free to copy it to your own project.

## Support ##

If you have any questions about the integration, please send them to appsdev@milesbreed.com